import mysql.connector
from mysql.connector import errorcode
from dotenv import load_dotenv
import os
load_dotenv()

TABLES = {}
TABLES['stats_matas'] = (
"CREATE TABLE `statistika` ("
" `Data` datetime NOT NULL,"
" `Serga` int(11) DEFAULT NULL,"
" `Pasveiko` int(11) DEFAULT NULL,"
" `Bendrai` int(11) DEFAULT NULL,"
" `PatvirtintiNaujai` int(11) DEFAULT NULL,"
" `PasveikoSiandien` int(11) DEFAULT NULL,"
" `Testai_siandien` int(11) DEFAULT NULL,"
" `Testai_total` int(11) DEFAULT NULL,"
" `Registruoti_total` int(11) DEFAULT NULL,"
"  PRIMARY KEY (`Data`))")


class db:
    def __init__(self):

        self.cnx = mysql.connector.connect(user=os.getenv("DB_USER"), password=os.getenv("DB_PASS"),
                                            host=os.getenv("DB_HOST"), port=os.getenv("DB_PORT"),
                                            database=os.getenv("DB_DB"))
        cursor = self.cnx.cursor()  
        for table_name in TABLES:
            table_description = TABLES[table_name]
        try:
            print("Creating table {}: ".format(table_name), end='')
            cursor.execute(table_description)
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                print("already exists.")
            else:
                print(err.msg)
        else:
            print("OK")

        cursor.close()  

    def get_stats(self):
        cursor = self.cnx.cursor()
        cursor.execute("Select * from statistika;")

        for i in cursor:
            print("{}".format(i))

        cursor.close()
    
    def get_latest(self):
        cursor = self.cnx.cursor(dictionary=True)
        cursor.execute("Select * from statistika where Data = (select max(Data) from statistika);")

        return cursor

    def add_stats(self, data):
        cursor = self.cnx.cursor()

        cursor.execute(
            "INSERT IGNORE INTO statistika"
            " (`Data`,`Serga`,`Pasveiko`,`Bendrai`,`PatvirtintiNaujai`,`PasveikoSiandien`,`Testai_siandien`,`Testai_total`,`Registruoti_total`,`Mirtys`,`Mirtys_siandien`)"
            " VALUES ( %(Data)s,%(Serga)s,%(Pasveiko)s,%(Bendrai)s,%(PatvirtintiNaujai)s,%(PasveikoSiandien)s,%(Testai_siandien)s,%(Testai_total)s,%(Registruoti_total)s,%(Mirtys),s%(Mirtys_siandien)s)", data)
        self.cnx.commit()

        cursor.close()

    def __del__(self):
        self.cnx.close()