from bs4 import BeautifulSoup
import requests
import re
import datetime
from db import db

storage = db()

def parseDate(raw_date):
    date = {
        "year": 2020,
        "month": None,
        "day": None,
        "hour": None,
        "min": None
    }
    ''' find month '''
    for m,l in lt_date.items():
        if l in raw_date:
            date['month'] = int(m)
    date['day'] = int(re.findall(".([\d]*)\sd\.",raw_date)[0])
    time_raw = re.findall(".([\d]*)\.([\d]*)\sval\.",raw_date)
    date['hour'] = int(time_raw[0][0])
    date['min'] = int(time_raw[0][1])
    print(date)
    return datetime.datetime(date['year'],date['month'],date['day'],date['hour'],date['min'])


lt_date = {
    1:"Sausio",
    2:"Vasario",
    3:"Kovo",
    4:"Balandžio",
    5:"Gegužės",
    6:"Birželio",
    7:"Liepos",
    8:"Rugpjūčio",
    9:"Rugsėjo",
    10:"Spalio",
    11:"Lapkričio",
    12:"Gruodžio"
}

parser = {
    "infected": "Lietuvoje patvirtintų ligos atvejų\:.([\d]*)",
    "test_perday": "Per vakar dieną ištirta\:.([\d]*).*$",
    "test_total": "Iki šiol iš viso ištirta ėminių dėl įtariamo koronaviruso\:.([\d]*).*$",
    "watching": "NVSC duomenis pateikusių asmenų skaičius\:.([\d]*)"
}

stt = {}

url = "http://sam.lrv.lt/lt/naujienos/koronavirusas"

def fetch():
    html_content = requests.get(url).text

    # Parse the html content
    soup = BeautifulSoup(html_content, "html.parser")

    stats = soup.find("div",attrs={"class": "text"})

    date_raw = stats.find_all("strong")[0]
    print(date_raw.text)
    stt['timestamp'] = parseDate(date_raw.text)


    stats_raw = stats.find("ul").find_all("li")

    for i in stats_raw:
        for k,p in parser.items():
            case = re.findall(p,i.text)
            if case:
                stt[k] = case[0].replace(" ", "")

    return stt
'''storage.add_stats(stt)

storage.get_stats()'''

curent_stats = fetch()
db_stats = list(storage.get_latest())

print(curent_stats)
print(db_stats[0])

if  db_stats[0]['Data'] < curent_stats['timestamp']:
    print("Updatinam i duombaze")
    new_stat = db_stats[0]
    new_stat['Data'] = curent_stats['timestamp']
    new_stat['PatvirtintiNaujai'] = int(curent_stats['infected']) - int(new_stat['Bendrai'])
    new_stat['Bendrai'] = curent_stats['infected']
    new_stat['Testai_siandien'] = curent_stats['test_perday']
    new_stat['Testai_total'] = curent_stats['test_total']
    new_stat['Registruoti_total'] = curent_stats['watching']
    new_stat['Serga'] = int(new_stat['Bendrai']) - int(new_stat['Pasveiko'])
    print("--- Formated new insert ---\n{}".format(new_stat))
    storage.add_stats(new_stat)
    print("--- Latest record in DB ---\n{}".format(list(storage.get_latest())))
else:
    print("jau pasene duomenys")
